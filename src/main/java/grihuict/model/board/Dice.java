package grihuict.model.board;

public interface Dice {
    int rollDice();
}
