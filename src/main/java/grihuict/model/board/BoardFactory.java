package grihuict.model.board;

import grihuict.model.json.*;

public class BoardFactory {
        public Board getInstance(GameName gameName) {
            JSONRead jsonRead;
            JSONCreation jsonCreation;
            Board board = null;
                switch (gameName) {
                    case Snake:
                        board = new BoardSnake();
                        jsonCreation = new JSONSnakeCreation();
                        jsonCreation.writeToJSON();
                        jsonRead = new JSONSnakeRead();
                        jsonRead.readFromJSON();
                        board.setPlayerList(jsonRead.getPlayers());
                        board.setSquareList(jsonRead.getSquares());
                        break;
                    case Foo:
                        board = new BoardFoo();
                        jsonCreation = new JSONFooCreation();
                        jsonCreation.writeToJSON();
                        jsonRead = new JSONFooRead();
                        jsonRead.readFromJSON();
                        board.setPlayerList(jsonRead.getPlayers());
                        board.setSquareList(jsonRead.getSquares());
                        break;
                    case Exit:
                        System.exit(0);
                        break;
                }
            return board;
        }
    }