package grihuict.model.board;

import grihuict.model.square.Square;

import java.util.List;

public interface Board {

    void setSquareList(List<Square> squareList);
    Player startGame();
    Result move(int diceNumber, Player p);
    Player nextPlayer(Player player);
    Player whoPlaysFirst();
    void setPlayerList(List<Player> players1);
}
