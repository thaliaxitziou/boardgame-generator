package grihuict.model.board;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class PlayerList {
    private List<Player> playerList = new ArrayList<>();

    public void setPlayerList(List<Player> playerList) {
        this.playerList = playerList;
    }

    public Player whoPlaysFirst() {
        for (Player player : playerList) player.rollDice();
        playerList.sort(Comparator.comparing(Player::getDiceNumber));
        return playerList.get(0);
    }

    public Player getNextPlayer(Player currentPlayer){

        Player playerToPlay;

        switch (currentPlayer.getPlayerTurnOptions()){
            case PLAY:
                playerToPlay=play(currentPlayer);
                break;
            case REMOVE:
                playerToPlay=remove(currentPlayer);
                break;
            case LOSE:
                playerToPlay=lose(currentPlayer);
                break;
            default:
                return playerList.get((playerList.indexOf(currentPlayer) + 1) % playerList.size());
        }
        return playerToPlay;
    }
    public Player play(Player currentPlayer){
        return playerList.get((playerList.indexOf(currentPlayer) + 1) % playerList.size());
    }
    public Player lose(Player currentPlayer){
        currentPlayer.setPlayerTurnOptions(PlayerTurnOptions.PLAY);
        return playerList.get((playerList.indexOf(currentPlayer) + 2) % playerList.size());
    }
    public Player remove(Player currentPlayer){
        if (playerList.size()>1)
            playerList.remove(currentPlayer);
        else
            System.out.println ("You are the only player left so keep playing");

        return playerList.get((playerList.indexOf(currentPlayer) + 1) % playerList.size());
    }
}
