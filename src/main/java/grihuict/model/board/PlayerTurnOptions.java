package grihuict.model.board;

public enum PlayerTurnOptions {
    PLAY,
    LOSE,
    REMOVE
}
