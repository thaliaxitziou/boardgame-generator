package grihuict.model.json;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class JSONSnakeCreation implements JSONCreation {

    private JSONObject j = new JSONObject();
    private JSONObject player1 = new JSONObject();
    private JSONObject player2 = new JSONObject();
    private JSONObject player3 = new JSONObject();
    private JSONObject square1 = new JSONObject();
    private JSONObject square2 = new JSONObject();
    private JSONObject square3 = new JSONObject();
    private JSONObject square4 = new JSONObject();
    private JSONObject square5 = new JSONObject();
    private JSONObject square6 = new JSONObject();
    private JSONObject square7 = new JSONObject();
    private JSONObject square8 = new JSONObject();
    private JSONObject square9 = new JSONObject();
    private JSONObject square10 = new JSONObject();
    private JSONObject square11 = new JSONObject();
    private JSONObject square12 = new JSONObject();
    private JSONObject square13 = new JSONObject();
    private JSONObject square14 = new JSONObject();
    private JSONObject square15 = new JSONObject();
    private JSONObject square16 = new JSONObject();
    private JSONObject square17 = new JSONObject();
    private JSONObject square18 = new JSONObject();
    private JSONObject square19 = new JSONObject();
    private JSONObject square20 = new JSONObject();
    @Override
    public void writeToJSON() {

        JSONArray playerList = new JSONArray();
        player1.put("position",0);
        player2.put("position",0);
        player3.put("position",0);
        player1.put("name","Markos");
        player2.put("name","Thalia");
        player3.put("name","Athina");
        playerList.add(player1);
        playerList.add(player2);
        playerList.add(player3);
        j.put("Players",playerList);
        JSONArray noPowerSquares = new JSONArray();
        square1.put("position",1);
        square2.put("position",2);
        square3.put("position",3);
        square4.put("position",4);
        square6.put("position",6);
        square8.put("position",8);
        square9.put("position",9);
        square10.put("position",10);
        square13.put("position",13);
        square14.put("position",14);
        square15.put("position",15);
        square17.put("position",17);
        square19.put("position",19);
        square20.put("position",20);
        noPowerSquares.add(square1);
        noPowerSquares.add(square2);
        noPowerSquares.add(square3);
        noPowerSquares.add(square4);
        noPowerSquares.add(square6);
        noPowerSquares.add(square8);
        noPowerSquares.add(square9);
        noPowerSquares.add(square10);
        noPowerSquares.add(square13);
        noPowerSquares.add(square14);
        noPowerSquares.add(square15);
        noPowerSquares.add(square17);
        noPowerSquares.add(square19);
        noPowerSquares.add(square20);
        JSONArray forwardSquares = new JSONArray();
        square5.put("position",5);
        square5.put("power",2);
        forwardSquares.add(square5);
        square11.put("position",11);
        square11.put("power",4);
        forwardSquares.add(square11);
        square16.put("position",16);
        square16.put("power",3);
        forwardSquares.add(square16);
        JSONArray backwardSquares = new JSONArray();
        square7.put("position",7);
        square7.put("power",3);
        backwardSquares.add(square7);
        square12.put("position",12);
        square12.put("power",8);
        backwardSquares.add(square12);
        square18.put("position",18);
        square18.put("power",4);
        backwardSquares.add(square18);
        j.put("NoPowerSquares",noPowerSquares);
        j.put("ForwardSquares",forwardSquares);
        j.put("BackWardSquares",backwardSquares);


        String fileName = "GameInfo.json";
        String path="C:\\Users\\thali\\IdeaProjects\\Test\\src\\\\main\\java\\grihuict\\model\\json";
        File dir = new File(path);
        try (FileWriter file = new FileWriter(new File(dir, fileName))) {
            file.write(j.toJSONString());
        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }
}
