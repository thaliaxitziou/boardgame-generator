package grihuict.model.json;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class JSONFooCreation implements JSONCreation {
    private JSONObject j = new JSONObject();
    private JSONObject player1 = new JSONObject();
    private JSONObject player2 = new JSONObject();
    private JSONObject player3 = new JSONObject();
    private JSONObject square1 = new JSONObject();
    private JSONObject square2 = new JSONObject();
    private JSONObject square3 = new JSONObject();
    private JSONObject square4 = new JSONObject();
    private JSONObject square5 = new JSONObject();
    private JSONObject square6 = new JSONObject();
    private JSONObject square7 = new JSONObject();
    private JSONObject square8 = new JSONObject();
    private JSONObject square9 = new JSONObject();
    private JSONObject square10 = new JSONObject();
    private JSONObject square11 = new JSONObject();
    private JSONObject square12 = new JSONObject();
    private JSONObject square13 = new JSONObject();
    private JSONObject square14 = new JSONObject();
    private JSONObject square15 = new JSONObject();
    private JSONObject square16 = new JSONObject();
    private JSONObject square17 = new JSONObject();
    private JSONObject square18 = new JSONObject();
    private JSONObject square19 = new JSONObject();
    private JSONObject square20 = new JSONObject();
    @Override
    public void writeToJSON() {

        JSONArray playerList = new JSONArray();
        player1.put("position",0);
        player2.put("position",0);
        player3.put("position",0);
        player1.put("name","Markos");
        player2.put("name","Thalia");
        player3.put("name","Athina");
        playerList.add(player1);
        playerList.add(player2);
        playerList.add(player3);
        j.put("Players",playerList);
        JSONArray addPointsSquare = new JSONArray();
        JSONArray losePointsSquare = new JSONArray();
        square1.put("position",1);
        square1.put("points",1);
        square3.put("position",3);
        square3.put("points",3);
        square4.put("position",4);
        square4.put("points",4);
        square6.put("position",6);
        square6.put("points",6);
        square8.put("position",8);
        square8.put("points",8);
        square10.put("position",10);
        square10.put("points",10);
        square13.put("position",13);
        square13.put("points",13);
        square14.put("position",14);
        square14.put("points",14);
        square15.put("position",15);
        square15.put("points",15);
        square17.put("position",17);
        square17.put("points",17);
        square19.put("position",19);
        square19.put("points",19);
        square20.put("position",20);
        square20.put("points",20);
        addPointsSquare.add(square1);
        addPointsSquare.add(square3);
        losePointsSquare.add(square4);
        addPointsSquare.add(square6);
        addPointsSquare.add(square8);
        losePointsSquare.add(square10);
        addPointsSquare.add(square13);
        losePointsSquare.add(square14);
        addPointsSquare.add(square15);
        losePointsSquare.add(square17);
        addPointsSquare.add(square19);
        addPointsSquare.add(square20);
        square2.put("position",2);
        square2.put("points",100);
        addPointsSquare.add(square2);
        square11.put("position",11);
        square11.put("points",150);
        addPointsSquare.add(square11);
        square5.put("position",5);
        square5.put("points",20);
        losePointsSquare.add(square5);
        square12.put("position",12);
        square12.put("points",50);
        losePointsSquare.add(square12);
        JSONArray loseTurnSquare = new JSONArray();
        square16.put("position",16);
        loseTurnSquare.add(square16);
        JSONArray cardSquare = new JSONArray();
        square7.put("position",7);
        square7.put("power","YELLOW");
        cardSquare.add(square7);
        square9.put("position",9);
        square9.put("power","RED");
        cardSquare.add(square9);
        square18.put("position",18);
        square18.put("power","YELLOW");
        cardSquare.add(square18);

        j.put("AddPointsSquares",addPointsSquare);
        j.put("LosePointsSquares",losePointsSquare);
        j.put("CardSquares",cardSquare);
        j.put("LoseTurnSquares",loseTurnSquare);


        String fileName = "GameFooInfo.json";
        String path = "C:\\Users\\thali\\IdeaProjects\\Test\\src\\main\\java\\grihuict\\model\\json";
        File dir = new File(path);
        try (FileWriter file = new FileWriter(new File(dir, fileName))) {
            file.write(j.toJSONString());
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
