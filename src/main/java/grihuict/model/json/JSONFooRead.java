package grihuict.model.json;

import grihuict.model.square.Card;
import grihuict.model.board.Player;
import grihuict.model.square.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class JSONFooRead implements JSONRead {
    private List<Player> players = new ArrayList<>();
    private List<Square> squares = new ArrayList<>();

    @Override
    public void readFromJSON()  {
        JSONParser jsonParser = new JSONParser();
        String pathOfGameInfo = "C:\\Users\\thali\\IdeaProjects\\Test\\src\\\\main\\java\\grihuict\\model\\json\\GameFooInfo.json";

        try (FileReader fileReader = new FileReader(pathOfGameInfo)) {
            Object obj = jsonParser.parse(fileReader);
            JSONObject jsonObject = (JSONObject) obj;

            JSONArray jsonArray = (JSONArray) jsonObject.get("Players");
            for (Object o : jsonArray) {
                JSONObject jsonObject1 = (JSONObject) o;
                players.add(new Player((int) (long) (jsonObject1.get("position")), (String) jsonObject1.get("name")));
            }
            JSONArray jsonArray3 = (JSONArray) jsonObject.get("AddPointsSquares");
            for (Object o : jsonArray3) {
                JSONObject jsonObject3 = (JSONObject) o;
                squares.add(new AddPointsSquare((int) (long) (jsonObject3.get("position")), (int) (long) jsonObject3.get("points")));
            }
            JSONArray jsonArray4 = (JSONArray) jsonObject.get("LosePointsSquares");
            for (Object o : jsonArray4) {
                JSONObject jsonObject4 = (JSONObject) o;
                squares.add(new LosePointsSquare((int) (long) (jsonObject4.get("position")), (int) (long) jsonObject4.get("points")));
            }
            JSONArray jsonArray5 = (JSONArray) jsonObject.get("CardSquares");
            for (Object o : jsonArray5) {
                JSONObject jsonObject5 = (JSONObject) o;
                squares.add(new CardSquare((int) (long) (jsonObject5.get("position")), Card.valueOf((String) jsonObject5.get("power"))));
            }
            JSONArray jsonArray6 = (JSONArray) jsonObject.get("LoseTurnSquares");
            for (Object o : jsonArray6) {
                JSONObject jsonObject6 = (JSONObject) o;
                squares.add(new LoseTurnSquare((int) (long) (jsonObject6.get("position"))));
            }
            squares.sort(Comparator.comparing(Square::getSquarePosition));

        } catch (ParseException | IOException e) {
            System.out.println("An unexpected error occurred please contact our support team!");
            System.exit(0);
            e.printStackTrace();
        }
    }

    @Override
    public List<Player> getPlayers() {
        return players;
    }

    @Override
    public List<Square> getSquares() {
        return squares;
    }




}
