package grihuict.model.square;

import grihuict.model.board.PlayerTurnOptions;
import grihuict.model.board.Player;

public class LoseTurnSquare implements Square {
    int position;

    @Override
    public String checkAndExecute(Player player) {
        player.setPlayerTurnOptions(PlayerTurnOptions.LOSE);
        return  "You lost your turn!!\n";
    }

    @Override
    public int getSquarePosition() {

        return position;
    }

    public LoseTurnSquare(int position) {
        this.position = position;
    }

    public String toString() {
        return "Position:" + position;
    }
}
