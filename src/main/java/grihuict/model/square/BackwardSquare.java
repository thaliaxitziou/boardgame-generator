package grihuict.model.square;

import grihuict.model.board.Player;

public class BackwardSquare implements Square {
    int position;
    int powerDown;

    public BackwardSquare(int position, int powerDown) {
        this.position = position;
        this.powerDown = powerDown;
    }

    @Override
    public String checkAndExecute(Player player) {
        player.setCurrentSquarePosition(player.getCurrentSquarePositionPosition() - powerDown);
        return  "Power Square\n Go back -" + powerDown + " positions\n";
    }

    @Override
    public int getSquarePosition() {

        return position;
    }

    public String toString() {
        return "Position:" + position + " PowerDown=" + powerDown;
    }
}
