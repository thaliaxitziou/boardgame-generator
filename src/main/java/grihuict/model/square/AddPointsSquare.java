package grihuict.model.square;


import grihuict.model.board.Player;

public class AddPointsSquare implements Square{
    int position;
    int point;

    public AddPointsSquare(int position, int point) {
        this.position = position;
        this.point = point;
    }

    @Override
    public String checkAndExecute(Player player)
    {
        int newPoints;
        newPoints = player.getPoints() + point;
        player.setPoints(newPoints);
        return "Add Points Square +" + point + " points.\n";
    }

    @Override
    public int getSquarePosition() {
        return position;
    }
}
