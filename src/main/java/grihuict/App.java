package grihuict;


import grihuict.model.board.*;

public class App
{
    public static void main( String[] args )
    {
        GameRoll gameRoll=new GameRoll();
        Player playerPlaying;
        Result result;
        BoardFactory boardConfiguration = new BoardFactory();
        ChooseGame chooseGame=new ChooseGame();
        Board board = boardConfiguration.getInstance(chooseGame.Display());
        playerPlaying = board.startGame();

            do
            {
                gameRoll.keepPlayingOrNot(playerPlaying);
                result = board.move(playerPlaying.rollDice(),playerPlaying);
                System.out.println(result.getMessage());
                playerPlaying=board.nextPlayer(playerPlaying);
        } while( result.getStatus() != Status.FINISH);
    }
}
